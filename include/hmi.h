#ifndef HMI_H
#define HMI_H

#include <QGraphicsView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <memory>
#include <QValidator>
#include <QTimer>
#include <QGraphicsPixmapItem>
#include <QDebug>
#include <QtSerialPort>
#include <QComboBox>

class HMI : public QGraphicsView
{
    Q_OBJECT
public:
    HMI();
    ~HMI() = default;
public slots:
    void Mute();
    void ChangeFrequency();
    void IncreaseFrequencyText();
    void DecreaseFrequencyText();
    void setFrequency();
    void setNewDate();
    void setNewTime();
    void ClearConsole();
    void UpdateFrequencyLabel();
    void SearchUp();
    void ConnectToUart();
    void SearchDown();
    void ReadAll();
    void updateStatusLabel(QString &newText);

private:
    std::unique_ptr<QGraphicsScene> Elements;
    std::unique_ptr<QLineEdit> FMSet;
    std::unique_ptr<QLineEdit> Day;
    std::unique_ptr<QLineEdit> Month;
    std::unique_ptr<QLineEdit> Year;
    std::unique_ptr<QLineEdit> Hours;
    std::unique_ptr<QLineEdit> Minutes;
    std::unique_ptr<QLineEdit> Seconds;


    std::unique_ptr<QPushButton> NewDate;
    std::unique_ptr<QPushButton> NewTime;

    std::unique_ptr<QPushButton> SetFrequencyButton;
    std::unique_ptr<QPushButton> ClearButton;
    std::unique_ptr<QPushButton> DecreaseFrequencyButton;
    std::unique_ptr<QPushButton> IncreaseFrequencyButton;
    std::unique_ptr<QPushButton> MuteButton;
    std::unique_ptr<QPushButton> SeekUpButton;
    std::unique_ptr<QPushButton> ConnectUart;
    std::unique_ptr<QPushButton> SeekDownButton;
    std::unique_ptr<QLabel> StatusLabel;
    std::unique_ptr<QLabel> ReceiveConsole;
    std::unique_ptr<QSerialPort> UART;
    std::unique_ptr<QComboBox> USB;
    std::unique_ptr<QGraphicsPixmapItem> pix;
    int LastFrequency{0};

    std::unique_ptr<QTimer> LabelUdater;
};


#endif // HMI_H

#include "../include/hmi.h"
#include <thread>
#include <chrono>

HMI::HMI()
{

    auto t = QSerialPortInfo::availablePorts();
    for(auto & a: t)
    {
        qDebug() << a.portName() << '\n';
    }
    UART = std::make_unique<QSerialPort>();

    UART->setBaudRate(115200,QSerialPort::AllDirections);
    UART->setDataBits(QSerialPort::Data8);
    UART->setParity(QSerialPort::NoParity);
    UART->setStopBits(QSerialPort::StopBits::OneStop);
    UART->setFlowControl(QSerialPort::FlowControl::NoFlowControl);
    pix = std::make_unique<QGraphicsPixmapItem>();
    pix->setPixmap(QPixmap(":/img/log.png"));
    pix->setPos(150,600);
    pix->setScale(0.5);



    QSerialPortInfo * UARTINFO = new QSerialPortInfo();
    QFont font ("arial");
    font.setPixelSize(30);
    LabelUdater=std::make_unique<QTimer>(this);
    LabelUdater->setInterval(1000);
    connect(LabelUdater.get(),SIGNAL(timeout()),this,SLOT(ReadAll()));
    QTimer * UARTReadTimer = new QTimer(this);
    UARTReadTimer->setInterval(500);
    connect(UARTReadTimer,SIGNAL(timeout()),this,SLOT(Rea()));
    LabelUdater->start();
    Elements=std::make_unique<QGraphicsScene>();
    Elements->setSceneRect(0,0,700,800);
    Elements->addItem(pix.get());
    StatusLabel = std::make_unique <QLabel> ();
    StatusLabel->setText(QString("Select port ->"));
    StatusLabel->setGeometry(200,60,300,30);
    StatusLabel->setAlignment(Qt::AlignmentFlag::AlignCenter);
    ReceiveConsole = std::make_unique <QLabel> ();
    ReceiveConsole->setText(QString(""));
    ReceiveConsole->setGeometry(130,350,470,200);
    ReceiveConsole->setAlignment(Qt::AlignmentFlag::AlignLeft);
    Elements->addWidget(ReceiveConsole.get());

    NewDate=std::make_unique<QPushButton>();
    NewDate->setGeometry(130,280,150,30);
    NewDate->setText(QString("SET NEW DATE"));
    NewTime=std::make_unique<QPushButton>();
    NewTime->setGeometry(450,280,150,30);
    NewTime->setText(QString("SET NEW TIME"));
    Elements->addWidget(NewTime.get());
    Elements->addWidget(NewDate.get());
    Elements->addWidget(StatusLabel.get());
    FMSet=std::make_unique<QLineEdit>();
    FMSet->setText(QString("Set own frequency here"));
    FMSet->setAlignment(Qt::AlignmentFlag::AlignHCenter);
    FMSet->setValidator(new QIntValidator(880,1080,this));
    FMSet->setGeometry(200,120,300,20);
    Elements->addWidget(FMSet.get());
    Day=std::make_unique<QLineEdit>();
    Day->setText(QString("DD"));
    Day->setAlignment(Qt::AlignmentFlag::AlignHCenter);
    Day->setValidator(new QIntValidator(1,31,this));
    Day->setFocusPolicy(Qt::NoFocus);
    Day->setAttribute(Qt::WA_TranslucentBackground);
    Day->setGeometry(130,250,30,20);
    Elements->addWidget(Day.get());

    Month=std::make_unique<QLineEdit>();
    Month->setText(QString("MM"));
    Month->setAlignment(Qt::AlignmentFlag::AlignHCenter);
    Month->setValidator(new QIntValidator(1,12,this));
    Month->setFocusPolicy(Qt::NoFocus);
    Month->setAttribute(Qt::WA_TranslucentBackground);
    Month->setGeometry(170,250,30,20);

    Elements->addWidget(Month.get());
    Year=std::make_unique<QLineEdit>();
    Year->setText(QString("YYYY"));
    Year->setAlignment(Qt::AlignmentFlag::AlignHCenter);
    Year->setValidator(new QIntValidator(1900,2050,this));
    Year->setFocusPolicy(Qt::NoFocus);
    Year->setAttribute(Qt::WA_TranslucentBackground);
    Year->setGeometry(210,250,50,20);

    Elements->addWidget(Year.get());
    Hours=std::make_unique<QLineEdit>();
    Hours->setText(QString("HH"));
    Hours->setAlignment(Qt::AlignmentFlag::AlignHCenter);
    Hours->setValidator(new QIntValidator(0,23,this));
    Hours->setFocusPolicy(Qt::NoFocus);
    Hours->setAttribute(Qt::WA_TranslucentBackground);
    Hours->setGeometry(450,250,30,20);

    Elements->addWidget(Hours.get());
    Minutes=std::make_unique<QLineEdit>();
    Minutes->setText(QString("MM"));
    Minutes->setAlignment(Qt::AlignmentFlag::AlignHCenter);
    Minutes->setValidator(new QIntValidator(0,59,this));
    Minutes->setFocusPolicy(Qt::NoFocus);
    Minutes->setAttribute(Qt::WA_TranslucentBackground);
    Minutes->setGeometry(490,250,30,20);

    Elements->addWidget(Minutes.get());
    Elements->addWidget(Hours.get());
    Seconds=std::make_unique<QLineEdit>();
    Seconds->setText(QString("SS"));
    Seconds->setAlignment(Qt::AlignmentFlag::AlignHCenter);
    Seconds->setValidator(new QIntValidator(0,59,this));
    Seconds->setFocusPolicy(Qt::NoFocus);
    Seconds->setAttribute(Qt::WA_TranslucentBackground);
    Seconds->setGeometry(530,250,30,20);

    Elements->addWidget(Seconds.get());
    MuteButton=std::make_unique<QPushButton>();
    SetFrequencyButton=std::make_unique<QPushButton>();
    SetFrequencyButton->setGeometry(200,160,300,30);
    SetFrequencyButton->setText(QString("SET FREQ"));
    Elements->addWidget(SetFrequencyButton.get());
    DecreaseFrequencyButton=std::make_unique<QPushButton>();
    DecreaseFrequencyButton->setGeometry(130,120,50,50);
    DecreaseFrequencyButton->setText(QString("-"));
    DecreaseFrequencyButton->setFont(font);
    DecreaseFrequencyButton->setFocusPolicy(Qt::NoFocus);
    DecreaseFrequencyButton->setAttribute(Qt::WA_TranslucentBackground);
    Elements->addWidget(DecreaseFrequencyButton.get());
    ConnectUart=std::make_unique<QPushButton>();
    ConnectUart->setGeometry(200,20,300,30);
    ConnectUart->setText(QString("Connect UART"));
    ConnectUart->setFont(font);
    ConnectUart->setFocusPolicy(Qt::NoFocus);
    ConnectUart->setAttribute(Qt::WA_TranslucentBackground);
    Elements->addWidget(ConnectUart.get());
    IncreaseFrequencyButton=std::make_unique<QPushButton>();
    IncreaseFrequencyButton->setGeometry(520,120,50,50);
    IncreaseFrequencyButton->setText(QString("+"));
    IncreaseFrequencyButton->setFont(font);
    IncreaseFrequencyButton->setFocusPolicy(Qt::NoFocus);
    IncreaseFrequencyButton->setAttribute(Qt::WA_TranslucentBackground);
    Elements->addWidget(IncreaseFrequencyButton.get());
    MuteButton->setGeometry(590,120,50,50);
    MuteButton->setText(QString("MUTE"));
    MuteButton->setFocusPolicy(Qt::NoFocus);
    MuteButton->setAttribute(Qt::WA_TranslucentBackground);
    Elements->addWidget(MuteButton.get());
    SeekUpButton=std::make_unique<QPushButton>();
    SeekUpButton->setGeometry(30,120,90,20);
    SeekUpButton->setText(QString("Seek Up"));
    SeekUpButton->setFocusPolicy(Qt::NoFocus);
    SeekUpButton->setAttribute(Qt::WA_TranslucentBackground);
    Elements->addWidget(SeekUpButton.get());
    ClearButton=std::make_unique<QPushButton>();
    ClearButton->setGeometry(30,350,90,20);
    ClearButton->setText(QString("Clear"));
    ClearButton->setFocusPolicy(Qt::NoFocus);
    ClearButton->setAttribute(Qt::WA_TranslucentBackground);
    Elements->addWidget(ClearButton.get());
    SeekDownButton=std::make_unique<QPushButton>();
    SeekDownButton->setGeometry(30,150,90,20);
    SeekDownButton->setText(QString("Seek Down"));
    SeekDownButton->setFocusPolicy(Qt::NoFocus);
    SeekDownButton->setAttribute(Qt::WA_TranslucentBackground);
    USB = std::make_unique <QComboBox> ();
    USB->addItems({"ttyUSB0","ttyUSB1","ttyUSB2","ttyUSB3", "ttyS4"});
    USB->setGeometry(510,60,100,30);
    Elements->addWidget(USB.get());

    Elements->addWidget(SeekDownButton.get());
    setScene(Elements.get());
    this->setWindowTitle("UAAART 2.0");
    QBrush background;
    background.setColor(QColor(205, 255, 201));
    background.setStyle(Qt::BrushStyle::SolidPattern);
    Elements->setBackgroundBrush(background);
    background.setColor(QColor(10,10,10));
    QPalette palette;
    palette.setColor(QPalette::Window, Qt::black);
    palette.setColor(QPalette::WindowText,Qt::white);

    ReceiveConsole->setPalette(palette);
    ReceiveConsole->setAutoFillBackground(true);
    ReceiveConsole->setText("Waiting for incoming messages");
    StatusLabel->setPalette(palette);
    StatusLabel->setAutoFillBackground(true);
    setFixedSize(static_cast<int>(Elements->width()),static_cast<int>(Elements->height()));
    setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    connect(MuteButton.get(),SIGNAL(released()),this,SLOT(Mute()));
     connect(ClearButton.get(),SIGNAL(released()),this,SLOT(ClearConsole()));
    connect(SetFrequencyButton.get(),SIGNAL(released()),this,SLOT(ChangeFrequency()));
    connect(IncreaseFrequencyButton.get(),SIGNAL(released()),this,SLOT(IncreaseFrequencyText()));
    connect(DecreaseFrequencyButton.get(),SIGNAL(released()),this,SLOT(DecreaseFrequencyText()));
    connect(SeekUpButton.get(),SIGNAL(released()),this,SLOT(SearchUp()));
    connect(NewDate.get(),SIGNAL(released()),this,SLOT(setNewDate()));
    connect(NewTime.get(),SIGNAL(released()),this,SLOT(setNewTime()));

    connect(SeekUpButton.get(),SIGNAL(released()),this,SLOT(SearchUp()));

    connect(SeekDownButton.get(),SIGNAL(released()),this,SLOT(SearchDown()));
    connect(ConnectUart.get(),SIGNAL(released()),this,SLOT(ConnectToUart()));
    //  LabelUdater->start();
    show();
    UARTReadTimer->start();
}

void HMI::Mute()
{
 if (MuteButton->text()=="MUTE")
 {

     if(0<UART->write("mute"))
     {
     StatusLabel->setText("Send mute message");
     }
     else
     {
     StatusLabel->setText("Error sending message");
     }
     MuteButton->setText("UNMUTE");
 }
 else
 {

     if(0<UART->write("unmute"))
     {
     StatusLabel->setText("Send unmute message");
     }
     else
     {
     StatusLabel->setText("Error sending message");
     }
     MuteButton->setText("MUTE");

 }


}


void HMI::ChangeFrequency()
{
    if (FMSet->text()=="Set own frequency here")
    {
        FMSet->setText(QString("880"));
    }
    auto a = FMSet->text();
    a.prepend("freq");
    if(0<UART->write(a.toStdString().c_str()))
    {

        a.prepend("Send message ");
        StatusLabel->setText(a);

    }
    else
    {
        StatusLabel->setText("Error sending FM");
    }

}

void HMI::IncreaseFrequencyText()
{
    if (FMSet->text()=="Set own frequency here")
    {
        FMSet->setText(QString("880"));
        return;
    }
    if (FMSet->text().toInt()>=1080)
    {
         FMSet->setText(QString("1080"));
        return;
    }
    int newfreq= FMSet->text().toInt()+1;
    FMSet->setText(QString::number(newfreq));
}

void HMI::DecreaseFrequencyText()
{
    if (FMSet->text()=="Set own frequency here")
    {
        FMSet->setText(QString("880"));
        return;
    }

    if (FMSet->text().toInt()<=880)
    {
        FMSet->setText(QString("880"));
        return;
    }

    int newfreq= FMSet->text().toInt()-1;
    FMSet->setText(QString::number(newfreq));
}

void HMI::setFrequency()
{
    if (FMSet->text()=="Set own frequency here")
    {
        FMSet->setText(QString("880"));
    }
    auto a = FMSet->text();
    a.prepend("fm");
    UART->write(a.toStdString().c_str());
    StatusLabel->setText(a);
}

void HMI::setNewDate()
{
    QString text = "ndt ";
    auto dday = Day->text() + " " +  Month->text() + " " + Year->text() + static_cast<char>(13);
    text+=dday;
    if( UART->write(text.toStdString().c_str()))
    {
    StatusLabel->setText("Send message " + text);
    }
    else
    {
     StatusLabel->setText("Failed sending " + text);
    }


}

void HMI::setNewTime()
{
    QString text = "ntt ";
    auto dday = Hours->text() + " " +  Minutes->text() + " " + Seconds->text()+static_cast<char>(13);
    text+=dday;

    if( UART->write(text.toStdString().c_str()))
    {
    StatusLabel->setText("Send message " + text);
    }
    else
    {
     StatusLabel->setText("Failed sending " + text);
    }


}

void HMI::ClearConsole()
{
    ReceiveConsole->clear();
}

void HMI::UpdateFrequencyLabel()
{


   // FMSet->setText(QString::number(LastFrequency));

   // StationCheck();

}

void HMI::SearchUp()
{

    auto text = "seekup";
    text+=static_cast<char>(13);
    if(0<UART->write(text))
    {
    StatusLabel->setText("Send Seek Up message");
    }
    else
    {
    StatusLabel->setText("Error sending message");
    }
}

void HMI::ConnectToUart()
{
    UART->setPortName(USB->currentText());
    if(UART->open(QIODevice::ReadWrite))
    {
        StatusLabel->setText("UART CONNECTED");
    }
    else
    {
       StatusLabel->setText("UART NOT CONNECTED");
    }

}

void HMI::SearchDown()
{


    auto text = "seekdown";
    text+=static_cast<char>(13);
    if(0<UART->write(text))
    {
    StatusLabel->setText("Send Seek Down message");
    }
    else
    {
    StatusLabel->setText("Error sending message");
    }


}

void HMI::ReadAll()
{
    QByteArray ar = UART->readAll();

    if(ar.size()>0)
    {
        qDebug() << "Date" << QTime::currentTime().toString() << " ";

    for (int var = 0; var < ar.size(); ++var)
    {
    qDebug() << static_cast<char>(ar[var]) ;
    }
    auto oldtext = ReceiveConsole->text();
    oldtext.prepend('\n');
    for (int i = ar.size();i>=0 ;i--)
    {

        oldtext.prepend(static_cast<char>(ar[i]));

    }
    ReceiveConsole->setText(oldtext);

    }
}



void HMI::updateStatusLabel(QString & newText)
{


        StatusLabel->setText(newText);




}


